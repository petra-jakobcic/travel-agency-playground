function Form1(person) {
    // Get the elements.
    const firstNameField = document.getElementById('first-name');
    const lastNameField = document.getElementById('last-name');

    // Set the initial value for the elements.
    firstNameField.value = person.getFirstName();
    lastNameField.value = person.getLastName();

    // Take the input values and add them to the person.
    this.updatePerson = () => {
        person.setFirstName(firstNameField.value);
        person.setLastName(lastNameField.value);
    };

    // Get the number of this form.
    this.getNumber = () => 1;

    this.hasNext = () => true;
};
