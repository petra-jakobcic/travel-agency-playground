function Person() {
    // Store the data internally in an object literal.
    const data = {
        firstName: '',
        lastName: ''
    };
    
    // Getters and setters.
    this.getFirstName = () => data.firstName;
    this.setFirstName = firstName => { data.firstName = firstName };
    
    this.getLastName = () => data.lastName;
    this.setLastName = lastName => { data.lastName = lastName };
    
    // Allow the data object to be converted to JSON (for submitting).
    this.toJson = () => JSON.stringify(data);
};
