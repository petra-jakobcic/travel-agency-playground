window.addEventListener('DOMContentLoaded', () => {
    const requestManager = new RequestManager();
    const person = new Person();

    const popup = new Popup(requestManager, person);
});
