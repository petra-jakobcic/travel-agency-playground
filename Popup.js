function Popup(requestManager, person) {
    const formNumberSpan = document.getElementById('span-number');
    const containerDiv = document.getElementById('div-container');
    const nextButton = document.getElementById('button-next');
    let currentForm = new Form1(person);

    const disableNextButton = () => {
        nextButton.disabled = true;
    };

    const enableNextButton = () => {
        nextButton.disabled = false;
    };

    const changeTheForm = formNumber => {
        switch (formNumber) {
            case 1:
                requestManager.getForm1(text => {
                    formNumberSpan.innerHTML = formNumber;
                    containerDiv.innerHTML = text;
                    currentForm = new Form1(person);
                    enableNextButton();
                });
            break;

            case 2:
                requestManager.getForm2(text => {
                    formNumberSpan.innerHTML = formNumber;
                    containerDiv.innerHTML = text;
                    currentForm = new Form2(person);
                    enableNextButton();
                });
            break;

            case 3:
                requestManager.getForm3(text => {
                    formNumberSpan.innerHTML = formNumber;
                    containerDiv.innerHTML = text;
                    currentForm = new Form3(person);
                });
            break;

            default:
                throw new Error('Invalid form number: ' + formNumber);
        }
    };

    nextButton.addEventListener('click', e => {
        disableNextButton();
        currentForm.updatePerson();

        if (currentForm.hasNext()) {
            changeTheForm(currentForm.getNumber() + 1);
        } else {
            requestManager.submitThePerson(person.toJson(), text => {
                containerDiv.parent.innerHTML = text;
            });
        }
    });
};
