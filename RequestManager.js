function RequestManager() {
    // Gets the text for page 1 and passes it into a callback that you give it.
    this.getPage1 = callback => {
        fetch('/page1').then(response => response.text()).then(text => {
            callback(text);
        });
    };

    this.getPage2 = callback => {
        fetch('/page2').then(response => response.text()).then(text => {
            callback(text);
        });
    };

    this.getPage3 = callback => {
        fetch('/page3').then(response => response.text()).then(text => {
            callback(text);
        });
    };

    this.submitThePerson = (personData, callback) => {
        fetch(
            '/person',
            {
                method: 'POST',
                data: personData,
                headers: {
                    "Content-Type": "application/json"
                }
            }
        ).then(response => response.text()).then(text => {
            callback(text);
        });
    }
};
